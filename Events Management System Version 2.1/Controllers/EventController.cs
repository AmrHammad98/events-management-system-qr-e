﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Events_Management_System_Version_2._1.Models;

namespace Events_Management_System_Version_2._1.Controllers
{
    public class EventController : Controller
    {
        // Instance of Database context
        private ApplicationDbContext context;

        // constructor 
        public EventController()
        {
            context = new ApplicationDbContext();
        }

        // dispose db context 
        protected override void Dispose(bool disposing)
        {
            context.Dispose();
        }

        [Authorize]
        public ActionResult AllEvents()
        {
            var events = context.Events.ToList();
            return View(events);
        }

        [Authorize]
        public ActionResult Single(int id)
        {
            var evnt = context.Events.SingleOrDefault(e => e.ID == id);
            return View(evnt);
        }

        // action result to enroll a user in an event
        [HttpPost]
        [Authorize]
        public ActionResult Enroll(Event evnt)
        {

            Event ev = context.Events.SingleOrDefault(e => e.ID == evnt.ID);
            ApplicationUser us = context.Users.FirstOrDefault();
            User_Event UE = new User_Event(ev, us);

            context.UserEvent.Add(UE);
            context.SaveChanges();
            return RedirectToAction("Index", "Home");
        }
    }
}
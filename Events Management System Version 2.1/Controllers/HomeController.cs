﻿using Events_Management_System_Version_2._1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Events_Management_System_Version_2._1.Controllers
{
    public class HomeController : Controller
    {
        // Instance of Database context
        private ApplicationDbContext context;

        // constructor 
        public HomeController()
        {
            context = new ApplicationDbContext();
        }

        // dispose db context 
        protected override void Dispose(bool disposing)
        {
            context.Dispose();
        }

        [AllowAnonymous]
        public ActionResult Index()
        {
            var events = context.Events.ToList();
            return View(events);
        }
    }
}
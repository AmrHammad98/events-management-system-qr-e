﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Events_Management_System_Version_2._1.Models;

namespace Events_Management_System_Version_2._1.Controllers
{
    public class UserEventController : Controller
    {
        private ApplicationDbContext context;

        // constructor 
        public UserEventController()
        {
            context = new ApplicationDbContext();
        }

        // dispose db context 
        protected override void Dispose(bool disposing)
        {
            context.Dispose();
        }

        public ActionResult MyEvents()
        {
            var UE = context.UserEvent.ToList();                    // user events 
            return View(UE);
        }

        public ActionResult SingleQR(int id)
        {
            var user_evnt = context.UserEvent.SingleOrDefault(ue => ue.ID == id);
            return View(user_evnt);
        }

    }
}
﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Events_Management_System_Version_2._1.Startup))]
namespace Events_Management_System_Version_2._1
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

﻿namespace Events_Management_System_Version_2._1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addUserEventTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.User_Event",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Event_ID = c.Int(),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Events", t => t.Event_ID)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id)
                .Index(t => t.Event_ID)
                .Index(t => t.User_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.User_Event", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.User_Event", "Event_ID", "dbo.Events");
            DropIndex("dbo.User_Event", new[] { "User_Id" });
            DropIndex("dbo.User_Event", new[] { "Event_ID" });
            DropTable("dbo.User_Event");
        }
    }
}

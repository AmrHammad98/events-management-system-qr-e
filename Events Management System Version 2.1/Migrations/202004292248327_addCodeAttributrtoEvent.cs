﻿namespace Events_Management_System_Version_2._1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addCodeAttributrtoEvent : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Events", "Code", c => c.String(nullable: false, maxLength: 6));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Events", "Code");
        }
    }
}

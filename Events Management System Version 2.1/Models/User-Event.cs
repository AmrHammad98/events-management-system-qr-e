﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Events_Management_System_Version_2._1.Models;

// this a bridge table to resolve the event user many to many relationship
namespace Events_Management_System_Version_2._1.Models
{
    public class User_Event
    {
        // attributes 
        public int ID { get; set; }                             // primary key 
        public virtual Event Event { get; set; }                // foreign key from events table
        public virtual ApplicationUser User { get; set; }       // foreign key from aspnetusers 

        // constructor 
        public User_Event()
        {

        }

        public User_Event(Event evnt, ApplicationUser usr)
        {
            this.Event = evnt;
            this.User = usr;
        }
    }
}